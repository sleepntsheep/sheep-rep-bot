#include <stdint.h>
#include <inttypes.h>
#include "now.h"

void on_now(struct discord *client, const struct discord_message *msg)
{
    uint64_t now_ms = discord_timestamp(client);
    char buf[64];
    snprintf(buf, sizeof buf, "<t:%" PRIu64 ">", now_ms / 1000);
    discord_create_message(client, msg->channel_id, 
            &(struct discord_create_message_params) { .content = buf }, NULL);
}

