#include "ping.h"

void on_ping(struct discord *client, const struct discord_message *msg)
{
    char buf[64];
    snprintf(buf, sizeof buf, "Client Ping: (%d ms)", discord_get_ping(client));
    discord_create_message(client, msg->channel_id,
        &(struct discord_create_message_params) { .content = buf }, NULL);
}

