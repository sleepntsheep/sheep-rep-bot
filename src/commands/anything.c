#include "anything.h"
#include <stddef.h>
#include <stdint.h>
#include <orca/discord.h>
#include <inttypes.h>
#include "../stb_ds.h"
#include "../Log.h"
#include "../Buffer.h"
#include <curl/curl.h>
#include <stdio.h>

char **anything_n = NULL;
#define N_MAX 3
#define MEMBER_SNPRINTF_ARG(m) m[0]->user->username, m[1]->user->username, m[2]->user->username

static CURL *curl = NULL;
static Buffer default_avatar = { 0 };
static uint64_t *excluded_member = NULL;

void anything_init(void)
{
    FILE *fp = fopen("anything/prompt", "r");
    if (!fp)
        warn("Failed to init anything");
    else  {
        char buf[1024];
        while (fgets(buf, sizeof buf, fp))
            arrpush(anything_n, strdup(buf));
        fclose(fp);
    }
    curl = curl_easy_init();
    if (!curl) warn("Failed to init curl");
    BufferReadFromFile(&default_avatar,
            "anything/defaultavatar.png");
    BufferPush(&default_avatar, "\x0", 1);
    FILE *fp2 = fopen("anything/exclude", "a+");
    if (!fp2) {
        warn("Failed to read exclude file");
    } else {
        char buf[1024];
        while (fgets(buf, sizeof buf, fp)) {
            uint64_t excid = strtoull(buf, NULL, 10);
            arrpush(excluded_member, excid);
        }
        fclose(fp2);
    }
}

void anything_cleanup(void)
{
    for (long i = 0; i < arrlen(anything_n); i++) {
        free(anything_n[i]);
    }
    arrfree(anything_n);
    if (curl) curl_easy_cleanup(curl);
    BufferCleanup(default_avatar);
    FILE *fp = fopen("anything/exclude", "w");
    if (!fp) {
        warn("Failed to save excluded member");
    } else {
        for (long i = 0; i < arrlen(excluded_member); i++) {
            char buf[1024];
            snprintf(buf, sizeof buf, "%"PRIu64"\n", excluded_member[i]);
            fputs(buf, fp);
        }
        fclose(fp);
    }
}

static bool is_excluded(const struct discord_user *user)
{
    for (long i = 0; i < arrlen(excluded_member); i++)
        if (excluded_member[i] == user->id)
            return true;
    return false;
}

static int select_random_member(struct discord *client, uint64_t guild_id, struct discord_guild_member **buf, size_t nmemb, struct discord_guild_member ***out_orig_list)
{
    /* todo - put this thing in hashmap, so no need to fetch members list every time this is called */
    ORCAcode res = discord_list_guild_members(client, guild_id, &(struct discord_list_guild_members_params) {
            .limit = 1000
    }, out_orig_list);
    if (res != ORCA_OK) {
        log_info("Failed listing guild member: %s", discord_strerror(res, client));
        return 0;
    }
    /* members is NULL terminated without length cache, that would make it slow */
    size_t l = 0;
    while ((*out_orig_list)[l]) l++;
    size_t i = 0;
    while (i < nmemb) {
        size_t sel = rand() % l;
        if ((*out_orig_list)[sel]->user == NULL)
            continue;
        if (is_excluded((*out_orig_list)[sel]->user))
            continue;
        buf[i++] = (*out_orig_list)[sel];
    }
    return i;
}

static int count_percents(char *s)
{
    if (!s || !*s) return 0;
    char *p = s + 1;
    int cnt = 0;
    while (*p)
    {
        if (*(p-1) == '%' && *p == 's')
            cnt++;
        p++;
    }
    return cnt;
}

static size_t writefunc(void *ptr, size_t size, size_t nmemb, void *s)
{
    BufferPush((Buffer*)s, ptr, size * nmemb);
    return nmemb;
}

static Buffer load_url(char *url)
{
    Buffer abuf;
    BufferInit(&abuf);
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writefunc);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &abuf);
    CURLcode res = curl_easy_perform(curl);

    if (res != CURLE_OK) {
        warn("Failed to request: %s", curl_easy_strerror(res));
        return (Buffer){ 0 };
    }
    return abuf;
}

void on_notme(struct discord *client, const struct discord_message *msg)
{
    if (is_excluded(msg->author)) {
        discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
                .content = "You are already on excluded list!"
        }, NULL);
    } else {
        arrpush(excluded_member, msg->author->id);
        discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
                .content = "Successfully added you to excluded list!"
        }, NULL);
    }
}

void on_anything(struct discord *client, const struct discord_message *msg)
{
    if (anything_n == NULL) return;
    size_t n = rand() % arrlen(anything_n);
    char *sel_format = anything_n[n];
    int n_people = count_percents(anything_n[n]);
    if (n_people > N_MAX) {
        warn("%s", "Too many %s in format string of anything!");
        return;
    }

    struct discord_guild_member *sel_member[N_MAX] = { 0 };
    struct discord_guild_member **member_list = NULL;

    if (select_random_member(client, msg->guild_id, sel_member, N_MAX, &member_list) < n_people)
        return;

    struct discord_embed embed = { .color = 0xFAFAFA,
                                   .timestamp = discord_timestamp(client) };

    char buf[1024];
    snprintf(buf, sizeof buf, sel_format, MEMBER_SNPRINTF_ARG(sel_member));

    discord_embed_set_title(&embed, buf);
    discord_embed_set_description(&embed, "If you are not comfortable with being "
            "possible randomized user on this command, do !notme to put your account "
            "in blacklist", NULL);

    struct discord_attachment atts[N_MAX + 1] = { 0 };
    Buffer avatars[N_MAX + 1] = { 0 };
    struct discord_attachment *attsl[N_MAX + 1] = { 0 };
    char *urldups[N_MAX + 1] = { 0 };

    for (int i = 0; i < n_people; i++)
    {
        discord_attachment_init(atts + i);
        snprintf(buf, sizeof buf, "https://cdn.discordapp.com/avatars/%"
                PRIu64"/%s.png", sel_member[i]->user->id, sel_member[i]->user->avatar);

        avatars[i] = load_url(buf);
        BufferPush(avatars + i, "\x0", 1);
        if (avatars[i].len > 1) {
            atts[i].size = avatars[i].len;
            atts[i].content = avatars[i].ptr;
        } else {
            atts[i].size = default_avatar.len;
            atts[i].content = default_avatar.ptr;
        }
        urldups[i] = strdup(buf);

        atts[i].id = i;
        atts[i].filename = buf;
        atts[i].content_type = "image/png";
        atts[i].url = atts[i].proxy_url = urldups[i];
        atts[i].ephemeral = false;
        if (atts[i].size == (size_t)-1)
            warn("Failed to get size for attachment#%zu", i);
        attsl[i] = atts + i;
    }

    attsl[n_people] = NULL;

    discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
        .embeds = (struct discord_embed *[]){ &embed, NULL },
        .attachments = attsl,
    }, NULL);

    for (size_t i = 0; i < N_MAX; i++) {
        BufferCleanup(avatars[i]);
        free(urldups[i]);
    }

    discord_guild_member_list_free(member_list);
}

