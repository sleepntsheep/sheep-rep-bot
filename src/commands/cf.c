#include <curl/curl.h>
#include <orca/discord.h>
#include <ctype.h>
#include "../Log.h"
#include "cf.h"
#include "../Buffer.h"
#include <stdlib.h>

struct cf_contest {
    char *id;
    char *name;
    char *time;
};

#define THIS_THING_IDK "<span class=\"format-time\" data-locale=\"en\">"
//#define THIS_THING2_IDK "<a href=\"/contestRegistration"

static int html_to_cf_contest(char *in, size_t in_bytes,
        struct cf_contest *buf, size_t buf_nmemb)
{
    /* write cf_contest to buf, if there are more contest than buf_nmemb
     * write buf_nmemb and return contest count 
     * return value - contest count */
    /* **param _in_ cannot be free-ed until you finish using cf_contest output!!!
     * this function does no allocation and cf_contest will store pointer to
     * each data in original buffer
     * ** param _in_ **will** be changed! */
    size_t buf_sp = 0;
    const char *line = in;
    for (; *line && line < in + in_bytes; buf_sp++) {
        char *d;
        struct cf_contest contest = { 0 };
        if (!(d = strstr(line, "data-contestId"))) break;
        d += 16;
        contest.id = d;
        if (!(d = strchr(d, '\"'))) break;
        *d = 0;
        if (!(d = strstr(d+1, "<td>"))) break;;
        d += 6;
        contest.name = d;
        if (!(d = strchr(d, '<'))) break;
        *d = 0;
        if (!(d = strstr(d + 1, THIS_THING_IDK))) break;
        d += sizeof THIS_THING_IDK - 1;
        contest.time = d;
        if (!(d = strchr(d, '<'))) break;
        *d = 0;
        line = d + 1;
        if (buf_sp < buf_nmemb) {
            buf[buf_sp] = contest;
        }
    }
    return buf_sp;
}

static size_t writefunc(void *ptr, size_t size, size_t nmemb, void *s)
{
    BufferPush((Buffer*)s, ptr, size * nmemb);
    return nmemb;
}

void on_cf_future(struct discord *client, const struct discord_message *msg)
{
    CURL *curl = curl_easy_init();
    if (!curl) {
        warn("Failed to init curl");
        return;
    }

    Buffer abuf;
    BufferInit(&abuf);
    curl_easy_setopt(curl, CURLOPT_URL, "https://codeforces.com/contests");
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writefunc);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &abuf);

    CURLcode res = curl_easy_perform(curl);

    if (res != CURLE_OK) {
        warn("Failed to request: %s", curl_easy_strerror(res));
        return;
    }

    struct cf_contest contests[10];
    size_t contests_count = html_to_cf_contest(abuf.ptr, abuf.len, contests, 10);
    if (contests_count > 10) contests_count = 10;

    struct discord_embed embed = { 
        .color = 0x3498DB,
        .timestamp = discord_timestamp(client)
    };

    discord_embed_set_title(&embed, "Codeforces Future Contests List.");
    discord_embed_set_description(&embed, "Next 10 future codeforces.com contests");
    discord_embed_set_url(&embed, "https://codeforces.com//contests");

    char buf[512];

    for (size_t i = 0; i < contests_count; i++) {
        snprintf(buf, sizeof buf,
                "[%s](https://codeforces.com/contestRegistration/%ld)\tAt: %s\n", contests[i].name,
                strtol(contests[i].id, NULL, 10), contests[i].time);
        discord_embed_add_field(&embed, contests[i].id, buf, true);
    }

    discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
        .embed = &embed,
    }, NULL);
    discord_embed_cleanup(&embed);

    curl_easy_cleanup(curl);
    BufferCleanup(abuf);
}

