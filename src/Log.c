#include "Log.h"

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

void __stderr_log(const char *type, const char *file, const int line,
                  const char *fmt, ...) {
    fprintf(stderr, "%s: %s:%d: ", type, file, line);
    va_list a;
    va_start(a, fmt);
    vfprintf(stderr, fmt, a);
    va_end(a);
    fprintf(stderr, "\n");
    fflush(stderr);
}


