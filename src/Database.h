#include <sqlite3.h>
#include "Log.h"

extern struct sqlite3 *database;

void Database_Init(void);
void Database_Cleanup(void);

#define CHECK_PREPARE \
        if (rc != SQLITE_OK) { \
            warn("Failed preparing stmt: %s", sqlite3_errmsg(database)); \
            return; \
        } \

#define CHECK_BIND \
        if (rc != SQLITE_OK) { \
            warn("Failed binding stmt: %s", sqlite3_errmsg(database)); \
            return; \
        } \

#define CHECK_FINALIZE \
        if (rc != SQLITE_OK) { \
            warn("Failed finalizing stmt: %s", sqlite3_errmsg(database)); \
            return; \
        } \


