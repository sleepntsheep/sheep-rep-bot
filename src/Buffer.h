#pragma once
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include "Log.h"

typedef struct Buffer {
    void *ptr; 
    size_t len;
    size_t cap;
} Buffer;

void BufferInit(Buffer *abuf);
void BufferPush(Buffer *abuf, void *ptr, size_t len);
void BufferCleanup(Buffer abuf);
int BufferReadFromFile(Buffer *abuf, const char *path);


