#include <stdlib.h>
#include <assert.h>
#include <inttypes.h>
#include "Hashtable.h"
#include "Uinfo.h"
#include "Log.h"

static Hashtable users_info;

void Uinfo_Init(void) {
    Hashtable_Init2(&users_info, 1000);
}

void Uinfo_Cleanup(void) {
    Hashtable_Cleanup(&users_info);
}

struct user_info *Uinfo_New(struct discord_user *user)
{
    struct user_info *info = malloc(sizeof *info);
    info->id = user->id;
    info->username = user->username;
    info->bsearch.playing = false;
    return info;
}

struct user_info *Uinfo_Get(struct discord_user *user)
{
    struct user_info *res = Hashtable_Get(&users_info, user->id);
    if (res == NULL) {
        res = Uinfo_New(user);
        info("Created new user %s", user->username);
        Hashtable_Insert(&users_info, user->id, res);
    }
    return res;
}

