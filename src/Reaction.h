#pragma once

#include <orca/discord.h>
#include <stdint.h>

void
ReactionOnAdd(struct discord *client, uint64_t user_id, uint64_t channel_id,
        uint64_t msg_id, uint64_t guild_id, const struct discord_guild_member *member,
        const struct discord_emoji *emoji);

void
ReactionOnRemove(struct discord *client, uint64_t user_id, uint64_t channel_id,
        uint64_t msg_id, uint64_t guild_id, const struct discord_emoji *emoji);

void
ReactionOnQuery(struct discord *client, const struct discord_message *msg);

