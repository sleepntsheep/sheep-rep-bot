#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <time.h>
#include "Stat.h"

static time_t bot_start = 0;

void Stat_Init(void)
{
    time(&bot_start);
}

void Stat_OnCommand(struct discord *client, const struct discord_message *msg)
{
    char buf[8096] = { 0 };
    uint64_t sys = 0;
    int temp = 0;

    FILE *fp = fopen("/proc/uptime", "r");
    fscanf(fp, "%"SCNu64, &sys);
    fclose(fp);

    fp = fopen("/sys/class/thermal/thermal_zone0/temp", "r");
    fscanf(fp, "%d", &temp);
    fclose(fp);

    temp /= 1000;
    uint64_t bot = time(0) - bot_start;

    snprintf(buf, sizeof buf, "System Temp: %d Celcius.\n"
            "System Uptime: %"PRIu64" seconds.\nBot Uptime: %"PRIu64" seconds",
            temp, sys, bot);

    discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
        .content = buf,
    }, NULL);
}

