#include "Hashtable.h"
#include <stdlib.h>

size_t Hashtable_KeyHash(Hashtable_Key k) {
    k = (k ^ (k >> 30)) * 0xbf58476d1ce4e5b9ULL;
    k = (k ^ (k >> 27)) * 0x94d049bb133111ebULL;
    k = k ^ (k >> 31);
    return k;
}

void Hashtable_Init2(Hashtable *ht, size_t alloc) {
    ht->alloc = alloc;
    ht->table = malloc(sizeof(ht->table[0]) * ht->alloc);
    ht->key = malloc(sizeof(ht->key[0]) * ht->alloc);
    Hashtable_Clear(ht);
}

void Hashtable_Init(Hashtable *ht) {
    ht->alloc = HASHTABLE_INITALLOC;
    ht->table = malloc(sizeof(ht->table[0]) * ht->alloc);
    ht->key = malloc(sizeof(ht->key[0]) * ht->alloc);
    Hashtable_Clear(ht);
}

void Hashtable_Insert(Hashtable *ht, Hashtable_Key k, void *v) {
    size_t h = Hashtable_KeyHash(k) % ht->alloc;
    for (size_t i = 0; i < ht->alloc; i++) {
        size_t t = (i + h) % ht->alloc;
        if (ht->table[t] == NULL) {
            ht->table[t] = v;
            ht->key[t] = k;
            return;
        }
    }

    Hashtable nht;
    Hashtable_Init2(&nht, ht->alloc * 2);
    for (size_t i = 0; i < ht->alloc; i++) {
        Hashtable_Insert(&nht, ht->key[i], ht->table[i]);
    }
    Hashtable_Insert(&nht, k, v);
    *ht = nht;
}

void *Hashtable_Get(Hashtable *ht, Hashtable_Key k) {
    size_t h = Hashtable_KeyHash(k) % ht->alloc;
    for (size_t i = 0; i < ht->alloc; i++) {
        size_t t = (i + h) % ht->alloc;
        if (ht->table[t] != NULL && ht->key[t] == k) {
            return ht->table[t];
        }
    }
    return NULL;
}

void *Hashtable_Delete(Hashtable *ht, Hashtable_Key k) {
    size_t h = Hashtable_KeyHash(k) % ht->alloc;
    for (size_t i = 0; i < ht->alloc; i++) {
        size_t t = (i + h) % ht->alloc;
        if (ht->table[t] != NULL && ht->key[t] == k) {
            void *v = ht->table[t];
            ht->table[t] = NULL;
            return v;
        }
    }
    return NULL;
}

void Hashtable_Update(Hashtable *ht, Hashtable_Key k, void *v) {
    size_t h = Hashtable_KeyHash(k) % ht->alloc;
    for (size_t i = 0; i < ht->alloc; i++) {
        size_t t = (i + h) % ht->alloc;
        if (ht->table[t] != NULL && ht->key[t] == k) {
            ht->table[t] = v;
            return;
        }
    }
}

void Hashtable_Clear(Hashtable *ht) {
    for (size_t i = 0; i < ht->alloc; i++)
        ht->table[i] = NULL;
}

void Hashtable_Cleanup(Hashtable *ht) {
    free(ht->table);
    free(ht->key);
}

