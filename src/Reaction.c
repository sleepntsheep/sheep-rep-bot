#include "Database.h"
#include "String.h"
#include "Reaction.h"
#include "Log.h"
#include <inttypes.h>
#include <orca/discord.h>

void
ReactionOnQuery(struct discord *client, const struct discord_message *msg)
{
	sqlite3_stmt *stmt = NULL;

	int rc = sqlite3_prepare_v2(database, "SELECT * FROM reaction WHERE guild_id LIKE (?)"
            " ORDER BY count DESC LIMIT 10", -1, &stmt, NULL);
    CHECK_PREPARE;

    rc = sqlite3_bind_int64(stmt, 1, (int64_t)msg->guild_id); // index start by 1 on bind
    CHECK_BIND;

    struct discord_embed embed = { 
        .color = 0x3498DB,
        .timestamp = discord_timestamp(client)
    };
    char buf[256], buf2[256];
    snprintf(buf, sizeof buf, "Top reaction on %"PRIi64, (int64_t)msg->guild_id);
    discord_embed_set_title(&embed, buf);

	while (sqlite3_step(stmt) == SQLITE_ROW) {
        uint64_t id = (uint64_t)sqlite3_column_int64(stmt, 1);
        char *name = (char*)sqlite3_column_text(stmt, 2);
        int count = sqlite3_column_int(stmt, 3);
        if (id != 0) {
            snprintf(buf2, sizeof buf2, "<:%s:%"PRIu64">", name, id);
        } else {
            snprintf(buf2, sizeof buf2, "%s", name);
        }
        snprintf(buf, sizeof buf, "%d", count);
        discord_embed_add_field(&embed, buf2, buf, true);
	}

    rc = sqlite3_finalize(stmt);
    CHECK_FINALIZE;

    discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
        .embed = &embed,
    }, NULL);
    discord_embed_cleanup(&embed);
}

void ReactionUpdate(const struct discord_emoji *emoji, uint64_t guild_id, int amount)
{
    sqlite3_stmt *stmt = NULL;
    int rc = sqlite3_prepare_v2(database, "SELECT * FROM reaction WHERE "
            "emoji_name LIKE (?) AND guild_id = (?)", -1, &stmt, NULL);
    CHECK_PREPARE;
    rc = sqlite3_bind_text(stmt, 1, emoji->name, -1, SQLITE_STATIC);
    CHECK_BIND;
    rc = sqlite3_bind_int64(stmt, 2, (int64_t)guild_id);
    CHECK_BIND;

    CHECK_PREPARE;

    rc = sqlite3_step(stmt);
    if (rc == SQLITE_ROW) {
        rc = sqlite3_finalize(stmt);
        CHECK_FINALIZE;

        String update_query = String_format("UPDATE reaction SET count = count + %d "
                "WHERE emoji_name LIKE (?) "
                "AND guild_id LIKE (?)", amount);
        rc = sqlite3_prepare_v2(database, update_query, -1, &stmt, NULL);
        CHECK_PREPARE;

        rc = sqlite3_bind_text(stmt, 1, emoji->name, -1, SQLITE_STATIC);
        CHECK_BIND;
        rc = sqlite3_bind_int64(stmt, 2, (int64_t)guild_id);
        CHECK_BIND;

        // Found, need to update
        rc = sqlite3_step(stmt);
        if (rc != SQLITE_DONE) {
            warn("Failed stepping stmt");
            return;
        }

        rc = sqlite3_finalize(stmt);
        CHECK_FINALIZE;
        String_free(update_query);
    } else {
        // Not found, insert emoji to table with count 1
        const char *insert_prompt = "INSERT INTO reaction "
            "(emoji_id, emoji_name, count, guild_id) VALUES (?, ?, ?, ?)";
        rc = sqlite3_prepare_v2(database, insert_prompt, -1, &stmt, NULL);
        CHECK_PREPARE;

        rc = sqlite3_bind_int64(stmt, 1, (int64_t)emoji->id);
        CHECK_BIND;
        rc = sqlite3_bind_text(stmt, 2, emoji->name, -1, SQLITE_STATIC);
        CHECK_BIND;
        rc = sqlite3_bind_int(stmt, 3, 1);
        CHECK_BIND;
        rc = sqlite3_bind_int64(stmt, 4, (int64_t)guild_id);
        CHECK_BIND;

        rc = sqlite3_step(stmt);
        if (rc != SQLITE_DONE) {
            warn("Failed stepping stmt");
            return;
        }
        rc = sqlite3_finalize(stmt);
        CHECK_FINALIZE;
    }
}

void
ReactionOnAdd(struct discord *client, uint64_t user_id, uint64_t channel_id,
        uint64_t msg_id, uint64_t guild_id, const struct discord_guild_member *member,
        const struct discord_emoji *emoji)
{
    (void)member;
    (void)client;
    (void)msg_id;
    (void)channel_id;
    (void)user_id;
    ReactionUpdate(emoji, guild_id, 1);
}

void
ReactionOnRemove(struct discord *client, uint64_t user_id, uint64_t channel_id,
        uint64_t msg_id, uint64_t guild_id, const struct discord_emoji *emoji)
{
    (void)client;
    (void)msg_id;
    (void)channel_id;
    (void)user_id;
    ReactionUpdate(emoji, guild_id, -1);
}



