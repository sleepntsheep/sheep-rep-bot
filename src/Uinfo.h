#pragma once
#ifndef UINFO_H_
#define UINFO_H_

#include <stdint.h>
#include <stddef.h>
#include <orca/discord.h>
#include "Bsearch.h"

struct user_info {
    uint64_t id;
    const char *username;
    struct bsearch bsearch;
};

void Uinfo_Init(void);
void Uinfo_Cleanup(void);
//struct user_info *Uinfo_new(struct discord_user *user);
struct user_info *Uinfo_Get(struct discord_user *user);

#endif

