#include "Notify.h"
#include "Database.h"
#include "String.h"
#include <inttypes.h>

void
Notify_OnMessage(struct discord *client, const struct discord_message *msg) {
    if (msg->author->bot) return;

	sqlite3_stmt *stmt = NULL;
	int rc = sqlite3_prepare_v2(database, "SELECT * FROM notify WHERE "
            "INSTR(?, substring) != 0 AND guild_id LIKE (?)", -1, &stmt, NULL);
    CHECK_PREPARE;
    rc = sqlite3_bind_text(stmt, 1, msg->content, -1, SQLITE_STATIC);
    CHECK_BIND;
    rc = sqlite3_bind_int64(stmt, 2, (int64_t)msg->guild_id);
    CHECK_BIND;

	while (sqlite3_step(stmt) == SQLITE_ROW) {
        char *substring = (char*)sqlite3_column_text(stmt, 1);
        uint64_t uid = (uint64_t)sqlite3_column_int64(stmt, 2);
        struct discord_channel dm;
        rc = discord_create_dm(client, &(struct discord_create_dm_params) {
                .recipient_id = uid,
        }, &dm);

        String cnt = String_format("%s was sent in <#%"PRIu64">,"
                " Link: https://discord.com/channels/%"PRIu64"/%"PRIu64"/%"PRIu64,
                substring, msg->channel_id, msg->guild_id, msg->channel_id, msg->id);

        discord_create_message(client, dm.id, &(struct discord_create_message_params) {
            .content = cnt,
        }, NULL);

        String_free(cnt);
        discord_channel_cleanup(&dm);
    }

    rc = sqlite3_finalize(stmt);
    CHECK_FINALIZE;
}

void
Notify_OnAdd(struct discord *client, const struct discord_message *msg) {
    if (msg->content[0] == 0) return;

    char *substr = msg->content;
    size_t substrlen = strlen(substr);
    if (substrlen > 256) {
        discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
                .content = "Notify keyword can't be longer than 256 bytes",
        }, NULL);
        return;
    }

	sqlite3_stmt *stmt = NULL;
	int rc = sqlite3_prepare_v2(database, "INSERT INTO notify "
            "(substring, user_id, guild_id) VALUES (?, ?, ?)", -1, &stmt, NULL);
    CHECK_PREPARE;
    rc = sqlite3_bind_text(stmt, 1, msg->content, -1, SQLITE_STATIC);
    CHECK_BIND;
    rc = sqlite3_bind_int64(stmt, 2, (int64_t)msg->author->id);
    CHECK_BIND;
    rc = sqlite3_bind_int64(stmt, 3, (int64_t)msg->guild_id);
    CHECK_BIND;

    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE) {
        warn("Failed stepping stmt");
        return;
    }

    rc = sqlite3_finalize(stmt);
    CHECK_FINALIZE;

    String reply = String_format("Added %s to your notify list", substr);
    discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
            .content = reply,
    }, NULL);
    String_free(reply);
}

void
Notify_OnList(struct discord *client, const struct discord_message *msg) {
    sqlite3_stmt *stmt = NULL;
	int rc = sqlite3_prepare_v2(database, "SELECT * FROM notify "
            "WHERE guild_id LIKE (?) AND user_id LIKE (?)", -1, &stmt, NULL);
    CHECK_PREPARE;
    rc = sqlite3_bind_int64(stmt, 1, (int64_t)msg->guild_id);
    CHECK_BIND;
    rc = sqlite3_bind_int64(stmt, 2, (int64_t)msg->author->id);
    CHECK_BIND;

    String reply = String_make("**Notify keyword list**\n");
    while (sqlite3_step(stmt) == SQLITE_ROW) {
        char *substring = (char*)sqlite3_column_text(stmt, 1);
        reply = String_cat(reply, substring);
        reply = String_cat(reply, "\n");
    }

    rc = discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
        .content = reply
    }, NULL);

    String_free(reply);
    rc = sqlite3_finalize(stmt);
    CHECK_FINALIZE;
}

void
Notify_OnRemove(struct discord *client, const struct discord_message *msg) {
    sqlite3_stmt *stmt = NULL;
	int rc = sqlite3_prepare_v2(database, "DELETE FROM notify "
            "WHERE guild_id LIKE (?) AND user_id LIKE (?) AND substring LIKE (?)", -1, &stmt, NULL);
    CHECK_PREPARE;
    rc = sqlite3_bind_int64(stmt, 1, (int64_t)msg->guild_id);
    CHECK_BIND;
    rc = sqlite3_bind_int64(stmt, 2, (int64_t)msg->author->id);
    CHECK_BIND;
    rc = sqlite3_bind_text(stmt, 3, msg->content, -1, SQLITE_STATIC);
    CHECK_BIND;

    String reply = NULL;

    if (sqlite3_step(stmt) == SQLITE_DONE) {
        reply = String_format("Successfully removed keyword %s", msg->content);
    } else {
        reply = String_format("Failed removing keyword: %s", sqlite3_errmsg(database));
    }

    rc = discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
        .content = reply,
    }, NULL);

    String_free(reply);
    rc = sqlite3_finalize(stmt);
    CHECK_FINALIZE;
}


