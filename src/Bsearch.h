#pragma once
#ifndef CMD_BSEARCH_H_
#define CMD_BSEARCH_H_
#include <orca/discord.h>

#define BSEARCH_MAX 20
#define BSEARCH_MIN 0

struct bsearch {
    uint64_t l, r, ans;
    uint64_t channel_id;
    int tries_left;
    bool playing;
};

void on_guess(struct discord *client, const struct discord_message *msg);
void on_bsearch(struct discord *client, const struct discord_message *msg);
 
#endif /* CMD_BSEARCH_H_ */

