#pragma once
#ifndef HASHTABLE_H_
#define HASHTABLE_H_
#include <stddef.h>

// TODO - test and make sure this work on collision

typedef unsigned long long Hashtable_Key;
#define HASHTABLE_INITALLOC (1 << 16)

typedef struct {
    size_t alloc;
    Hashtable_Key *key;
    void **table;
} Hashtable;

void Hashtable_Init(Hashtable *ht);
void Hashtable_Init2(Hashtable *ht, size_t alloc);
void Hashtable_Insert(Hashtable *ht, Hashtable_Key k, void *v);
void *Hashtable_Get(Hashtable *ht, Hashtable_Key k);
void *Hashtable_Delete(Hashtable *ht, Hashtable_Key k);
void Hashtable_Update(Hashtable *ht, Hashtable_Key k, void *v);
void Hashtable_Clear(Hashtable *ht);
void Hashtable_Cleanup(Hashtable *ht);

#endif /* HASHTABLE_H_ */

