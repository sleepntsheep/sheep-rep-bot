#include "Buffer.h"

void BufferInit(Buffer *abuf)
{
    abuf->ptr = malloc(4);
    abuf->cap = 4;
    abuf->len = 0;
}

void BufferPush(Buffer *abuf, void *ptr, size_t len)
{
    while (abuf->len + len > abuf->cap) {
        abuf->ptr = realloc(abuf->ptr, abuf->cap *= 2);
        if (abuf->ptr == NULL) {
            warn("Failed to realloc");
            return;
        }
    }
    memcpy((unsigned char*)abuf->ptr + abuf->len, ptr, len);
    abuf->len += len;
}

void BufferCleanup(Buffer abuf)
{
    free(abuf.ptr);
}

int BufferReadFromFile(Buffer *abuf, const char *path)
{
    BufferInit(abuf);
    FILE *f = fopen(path, "r");
    if (!f) return 1;
    fseek(f, 0L, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0L, SEEK_SET);

    abuf->cap = fsize + 1;
    abuf->len = fsize;
    abuf->ptr = realloc(abuf->ptr, abuf->cap);

    size_t readed = fread(abuf->ptr, fsize, 1, f);

    fclose(f);
    return readed != 1;
}

