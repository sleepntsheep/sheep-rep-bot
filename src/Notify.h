#pragma once

#include <orca/discord.h>
#include <stdint.h>

void
Notify_OnMessage(struct discord *client, const struct discord_message *msg);

void
Notify_OnAdd(struct discord *client, const struct discord_message *msg);

void
Notify_OnList(struct discord *client, const struct discord_message *msg);

void
Notify_OnRemove(struct discord *client, const struct discord_message *msg);

