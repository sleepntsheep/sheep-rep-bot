#include "Bsearch.h"
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include "Uinfo.h"

void on_guess(struct discord *client, const struct discord_message *msg)
{
    struct user_info *uinfo = Uinfo_Get(msg->author);
    if (!uinfo->bsearch.playing || msg->channel_id != uinfo->bsearch.channel_id) {
        return;
    }
    char buf[128];
    errno = 0;
    uint64_t guess = strtoull(msg->content, NULL, 10);
    if (errno) {
        return;
    }
    uinfo->bsearch.tries_left--;
    if (guess > uinfo->bsearch.ans) {
        snprintf(buf, sizeof buf, "%"PRIu64" was too high! %d tries left.", guess,
                uinfo->bsearch.tries_left);
    } else if (guess < uinfo->bsearch.ans) {
        snprintf(buf, sizeof buf, "%"PRIu64" was too low! %d tries left.", guess,
                uinfo->bsearch.tries_left);
    } else {
        snprintf(buf, sizeof buf, "%"PRIu64" was correct! 🙇 กราบครับ ยอมแล้วครับ.", guess);
        uinfo->bsearch.playing = false;
    }
    if (uinfo->bsearch.tries_left == 0 && guess != uinfo->bsearch.ans) {
        strcat(buf, " You lost. 🥹");
        uinfo->bsearch.playing = false;
    }
    discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
        .content = buf,
    }, NULL);
}

void on_bsearch(struct discord *client, const struct discord_message *msg)
{
    struct user_info *uinfo = Uinfo_Get(msg->author);
    if (!strcmp(msg->content, "stop")) {
        uinfo->bsearch.playing = false;
        discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
            .content = "Stopped bsearch!",
        }, NULL);
        return;
    }
    char buf[256];
    uint64_t l = BSEARCH_MIN;
    uint64_t r = BSEARCH_MAX;
    if (msg->content[0]) {
        r = strtoull(msg->content, NULL, 10);
    }
    uinfo->bsearch.channel_id = msg->channel_id;
    uinfo->bsearch.l = l;
    uinfo->bsearch.r = r;
    uinfo->bsearch.tries_left = (int)log2(r - l + 1) + 1;
    uinfo->bsearch.playing = true;
    uinfo->bsearch.ans = (rand() ^ (rand() << 15) ^ ((uint64_t) rand() << 30)) % r;
    snprintf(buf, sizeof buf, "Started bsearch!! min: %"PRIu64", max: %"PRIu64". !guess <num> to start playing. You have %d tries.", l, r, uinfo->bsearch.tries_left);
    discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
        .content = buf,
    }, NULL);
}



