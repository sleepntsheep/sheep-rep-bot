#include "Database.h"
#include "Log.h"
#include <stddef.h>

struct sqlite3 *database;

void Database_Init(void) {
    if (sqlite3_open_v2("data.db", &database, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL) != SQLITE_OK) {
        panic("Failed opening db: %s", sqlite3_errmsg(database));
    }

    char *err = NULL;

    sqlite3_exec(database, "CREATE TABLE IF NOT EXISTS "
            "reaction(id INTEGER PRIMARY KEY AUTOINCREMENT, emoji_id INT, emoji_name TEXT, count INT, guild_id INTEGER)",
            NULL, NULL, &err);

    sqlite3_exec(database, "CREATE TABLE IF NOT EXISTS "
            "notify(id INTEGER PRIMARY KEY AUTOINCREMENT, substring TEXT, user_id INTEGER, guild_id INTEGER)",
            NULL, NULL, &err);

    if (err) {
        panic("Failed creating table %s", sqlite3_errmsg(database));
    }
}

void Database_Cleanup(void) {
    sqlite3_close_v2(database);
}

