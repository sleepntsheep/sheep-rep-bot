#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <inttypes.h>
#include <sqlite3.h>
#include <orca/discord.h>
#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"
#include "commands/ping.h"
#include "commands/now.h"
#include "commands/anything.h"
#include "commands/cf.h"
#include "Database.h"
#include "Log.h"
#include "Notify.h"
#include "Reaction.h"
#include "Uinfo.h"
#include "Bsearch.h"
#include "Stat.h"

#define VERSION "0.1.1"
#define LENGTH(a) (sizeof (a) / sizeof (*(a)))

#define OWNER 428101976597725184LL

void on_ready(struct discord *client) 
{
    const struct discord_user *bot = discord_get_self(client);
    log_info("Bot logged in as %s!", bot->username);

    discord_set_presence(client, &(struct discord_presence_status) {
        .activities = (struct discord_activity *[]){
            &(struct discord_activity){
                .name = "Worshipping ShinLena",
                .type = DISCORD_ACTIVITY_GAME
            },
            NULL
        },
        .status = "online",
        .since = discord_timestamp(client)
    });
}

void on_presence(struct discord *client, const struct discord_message *msg)
{
    if (msg->author->id != OWNER) return;
    discord_set_presence(client, &(struct discord_presence_status) {
        .activities = (struct discord_activity *[]){
            &(struct discord_activity){
                .name = msg->content,
                .type = DISCORD_ACTIVITY_GAME
            },
            NULL
        },
        .status = "online",
        .since = discord_timestamp(client)
    });
}

void on_kill(struct discord *client, const struct discord_message *msg) {
    if (msg->author->id != 428101976597725184LL) {
        discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
            .content = "No permission 🤡🤡🤡",
        }, NULL);
    } else {
        discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
            .content = "Suiciding 😭😭😭",
        }, NULL);
        info("!kill toggled, exiting!!");
        exit(0);
    }
}

void on_help(struct discord *client, const struct discord_message *msg);

struct {
    char *cmd;
    char *desc;
    discord_on_message cb;
} commands[] = {
    {"bsearch", "Binary search game", &on_bsearch},
    {"timestamp", "Get current discord timestamp", &on_now},
    {"ant", "Send random prompt", &on_anything},
    {"cff", "List codeforce future competition", &on_cf_future},
    //{"notme", "Remove yourself from ant command ran, &on_notme},
    {"top_react", "Check 10 most reacted emoji on the server", &ReactionOnQuery},
    {"notify_add", "Add new keyword to your notify (get a dm whenever"
                " a message containing that keyword is sent)", &Notify_OnAdd},
    {"notify_list", "List all your notify keyword", &Notify_OnList},
    {"notify_remove", "Remove notify keyword, first argument is keyword to remove", &Notify_OnRemove},
    {"ping", "Check bot's ping", &on_ping},
    {"stat", "Check bot's stat", &Stat_OnCommand},
    {"kill", "Admin only: Kill bot", &on_kill},
    {"presence", "Admin only: Change bot presence", &on_presence},
    {"help", "show this help message", &on_help},
};

void on_help(struct discord *client, const struct discord_message *msg) {
    struct discord_embed embed = { 
        .color = 0x3498DB,
        .timestamp = discord_timestamp(client)
    };
    discord_embed_set_title(&embed, "Bot command list");

    for (size_t i = 0; i < LENGTH(commands); i++) {
        discord_embed_add_field(&embed, commands[i].cmd, commands[i].desc, false);
    }

    discord_create_message(client, msg->channel_id, &(struct discord_create_message_params) {
        .embed = &embed,
    }, NULL);
    discord_embed_cleanup(&embed);
}

int main(void)
{
    orca_global_init();
    atexit(orca_global_cleanup);
    srand(time(NULL));
    anything_init();
    atexit(anything_cleanup);
    Stat_Init();

    Uinfo_Init();
    atexit(Uinfo_Cleanup);

    Database_Init();
    atexit(Database_Cleanup);

    struct discord *client = discord_config_init("config.json");
    if (!client) panic("Couldn't initialize client");

    discord_set_on_ready(client, &on_ready);

    discord_set_on_message_create(client, &Notify_OnMessage);
    discord_set_on_message_reaction_remove(client, &ReactionOnRemove);
    discord_set_on_message_reaction_add(client, &ReactionOnAdd);
    discord_set_on_message_create(client, on_guess);

    for (size_t i = 0; i < LENGTH(commands); i++) {
        discord_set_on_command(client, commands[i].cmd, commands[i].cb);
    }

    discord_run(client);
    discord_cleanup(client);
    return 0;
}

