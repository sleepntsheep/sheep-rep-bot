CC := c99
CFLAGS := -Wall -Wextra -pedantic -g -D_POSIX_C_SOURCE=200809L
LDFLAGS := -L/usr/local/lib -ldiscord -lcurl -lm -lpthread -lsqlite3
SRCS := src/*.c src/commands/*.c

sheep-rep: $(SRCS)
	$(CC) -o sheep-rep $(SRCS) $(CFLAGS) $(LDFLAGS)

